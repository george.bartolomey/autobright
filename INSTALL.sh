#!/bin/sh

install -o root -m 755 autobright /usr/bin/ && \
install -o root -m 711 autobright.service /usr/lib/systemd/system/ && \
echo Done!
