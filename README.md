# Autobright

My brother had a problem on his laptop: in random moment brightness of the screen decreases down to zero.

I wrote this script to fix this problem!

## Installation

Run INSTALL.sh as root.

Enable service `autobright.service` by running `systemctl enable --now autobright.service`.

## Configuration

You may set brightness level in percents by editing file `/etc/brightness`
